package com.example.teamcovoit

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity

/**
 * Created by Onilax on 13/04/2017.
 */

class SplashActivity : AppCompatActivity() {
    @Override
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val intentLogin = Intent(this,LoginActivity::class.java)
        startActivity(intentLogin)
        finish()
    }
}