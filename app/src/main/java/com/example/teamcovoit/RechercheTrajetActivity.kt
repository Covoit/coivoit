package com.example.teamcovoit

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_recherche_trajet.*

class RechercheTrajetActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recherche_trajet)
        val intentPageListeCovoitActivity : Intent =  Intent(this,CovoitMaps::class.java)

        buttonListeCovoit.setOnClickListener {
            startActivity(intentPageListeCovoitActivity)
        }
    }
}
