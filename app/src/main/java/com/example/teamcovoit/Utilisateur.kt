package com.example.teamcovoit

import android.os.Build
import androidx.annotation.RequiresApi

@RequiresApi(Build.VERSION_CODES.N)
// id en val car non modifiable -> le reste en var pour update
class Utilisateur(val id: String,var prenom: String, var nom: String, var email: String, var tel: String) {

    init {
        // exécuté à l'init
        println("New user created : $prenom $nom. Contacts : Tél -> $tel Mail -> $email")
    }

    companion object{
        // simulation de base pour test
        private var users = mutableListOf<Utilisateur>()
        // utilisateur courant (récupérable partout dans le projet)
        var user = Utilisateur("a","","","","")

        // creation d'un nouvel utiliosateur en base
        fun create(id: String, prenom: String, nom: String, email: String, tel: String): Utilisateur
        {
            // si l'utilisateur n'existe pas deja
            if(GetDonneBDD().getDonnees("http://student.ddadev.fr/dev_mobile/Projet_Dev/Team_Covoit/API/getUtilisateurById.php?id=$id").length() == 0)
            {
                GetDonneBDD().postDonnees("http://student.ddadev.fr/dev_mobile/Projet_Dev/Team_Covoit/API/postUtilisateur.php?id=${id}&nom=${nom}&prenom=${prenom}&email=${email}&tel=${tel}")
            }
            // alimentation de la variable contenant l'utilisateur courant a tout moment
            user = Utilisateur(id, prenom, nom, email, tel)
            if(!users.contains(Utilisateur(id, prenom, nom, email, tel)))
            {
                users.add(user)
            }
            // retour non utilisé pour le moment
            return user
        }

        // pour mise à jour du téléphone
        fun updatePhone(id: String, prenom: String, nom: String, email: String, tel: String)
        {
            GetDonneBDD().postDonnees("http://student.ddadev.fr/dev_mobile/Projet_Dev/Team_Covoit/API/updateUtilisateur.php?id=${id}&nom=${nom}&prenom=${prenom}&email=${email}&tel=${tel}")
            users.find { a -> a.id == id }?.tel = tel
        }

        // recupération d'un utilisateur en fonction de son id
        fun getUserById(uid: String) : Utilisateur?
        {
            val res = GetDonneBDD().getDonnees("http://student.ddadev.fr/dev_mobile/Projet_Dev/Team_Covoit/API/getUtilisateurById.php?id=$uid")
            val id = res.getJSONObject(0)["id"].toString()
            val prenom = res.getJSONObject(0)["prenom"].toString()
            val nom = res.getJSONObject(0)["nom"].toString()
            val email = res.getJSONObject(0)["email"].toString()
            val tel = res.getJSONObject(0)["telephone"].toString()
            return Utilisateur(id, prenom, nom, email, tel)
        }
    }
}