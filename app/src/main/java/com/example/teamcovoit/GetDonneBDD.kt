package com.example.teamcovoit

import android.os.Build
import android.os.StrictMode
import org.json.JSONArray
import androidx.annotation.RequiresApi
import com.google.android.gms.maps.model.LatLng
import java.net.HttpURLConnection
import java.net.URL

class GetDonneBDD {

    @RequiresApi(Build.VERSION_CODES.N)

    // fonction permettant de récuperer des donnes de la base de données
    fun getDonnees(lienURL: String): JSONArray {

        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()

        StrictMode.setThreadPolicy(policy)
        var url = URL(lienURL)
        var resultat: JSONArray = JSONArray()
        with(url.openConnection() as HttpURLConnection) {
            requestMethod = "GET"  // optional default is GET
            println("### Après request method")
            inputStream.bufferedReader().use {
                it.lines().forEach { line ->
                    resultat = JSONArray(line)

                    println("resultat = " + resultat)
                }
            }
        }
        return resultat
    }

    // fonction permettant d'insérer des données dans la base de données
    @RequiresApi(Build.VERSION_CODES.N)
    fun postDonnees(lienURL: String){
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        var url = URL(lienURL)
        var resultat: JSONArray = JSONArray()
        var listPointDepartTrajet = arrayListOf<LatLng>()
        with(url.openConnection() as HttpURLConnection) {
            requestMethod = "POST"  // optional default is GET
            inputStream.bufferedReader().use {
                println("Lecture du resultat")
            }
        }
    }

    // récupération de l'id le plus élevé de la table Adresse
    @RequiresApi(Build.VERSION_CODES.N)
    fun getMaxAdresses(): Int{
        var liendeURL: String = "http://student.ddadev.fr/dev_mobile/Projet_Dev/Team_Covoit/API/getMaxAdresses.php"
        var resultatRequete = getDonnees(liendeURL)
        if(resultatRequete != null) {
            var maxID: Int = 0
            for (i in 0..resultatRequete.length() - 1) {
                //id,nom,prenom,tel,mail
                maxID = resultatRequete.getJSONObject(i)["MAX(id)+1"].toString().toInt()
            }
            return maxID
        }
        return 0
    }

    // récupération de l'id le plus élevé de la table Trajet
    @RequiresApi(Build.VERSION_CODES.N)
    fun getMaxTrajets(): Int{
        var liendeURL: String = "http://student.ddadev.fr/dev_mobile/Projet_Dev/Team_Covoit/API/getMaxTrajets.php"
        var resultatRequete = getDonnees(liendeURL)
        if(resultatRequete != null) {
            var maxID: Int = 0
            for (i in 0..resultatRequete.length() - 1) {
                //id,nom,prenom,tel,mail
                maxID = resultatRequete.getJSONObject(i)["MAX(id)+1"].toString().toInt()
            }
            return maxID
        }
        return 0
    }

    // récupération des données relatives aux Trajets, User et Adresses
    @RequiresApi(Build.VERSION_CODES.N)
    fun getTrajets(): List<Trajet>{

        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()

        StrictMode.setThreadPolicy(policy)

        var resultatTrajet: JSONArray = JSONArray()
        var resultatUser: JSONArray = JSONArray()
        var resultatAdresse: JSONArray = JSONArray()

        resultatTrajet = getDonnees("http://student.ddadev.fr/dev_mobile/Projet_Dev/Team_Covoit/API/getListeTrajet.php")
        resultatUser = getDonnees("http://student.ddadev.fr/dev_mobile/Projet_Dev/Team_Covoit/API/getListeUtilisateurs.php")
        resultatAdresse = getDonnees("http://student.ddadev.fr/dev_mobile/Projet_Dev/Team_Covoit/API/getListeAdresse.php")

        var listeUtilisateur : ArrayList<Utilisateur> = ArrayList()
        var listeAdresse : ArrayList<Adresse> = ArrayList()
        var listeTrajet : ArrayList<Trajet> = ArrayList()


        if(resultatUser != null)
        {
            // Traitement du JSON
            for(i in 0..resultatUser.length()-1)
            {
                //id,nom,prenom,tel,mail
                var monId : String = resultatUser.getJSONObject(i)["id"].toString()
                var monNom : String = resultatUser.getJSONObject(i)["nom"].toString()
                var monPrenom : String = resultatUser.getJSONObject(i)["prenom"].toString()
                var monTel : String = resultatUser.getJSONObject(i)["telephone"].toString()
                var monMail : String = resultatUser.getJSONObject(i)["email"].toString()

                var monUtilisateur : Utilisateur = Utilisateur(monId,monNom,monPrenom,monMail,monTel)
                listeUtilisateur.add(monUtilisateur)

            }
        }

        if(resultatAdresse != null)
        {
            // Traitement du JSON
            for(i in 0..resultatAdresse.length()-1)
            {
                //id,ville,lat,long
                var monId : Int = resultatAdresse.getJSONObject(i)["id"].toString().toInt()
                var maVille : String = resultatAdresse.getJSONObject(i)["ville"].toString()
                var maLatitude : Double = resultatAdresse.getJSONObject(i)["positionLat"].toString().toDouble()
                var maLongitude : Double = resultatAdresse.getJSONObject(i)["positionLng"].toString().toDouble()

                var monAdresse : Adresse = Adresse(monId,maVille,maLatitude,maLongitude)
                listeAdresse.add(monAdresse)


            }
        }

        if(resultatTrajet != null)
        {
            // Traitement du JSON
            for(i in 0..resultatTrajet.length()-1)
            {

                var monId : Int = resultatAdresse.getJSONObject(i)["id"].toString().toInt()
                var monConducteur : Utilisateur = Utilisateur("","","","","")
                var monAdressedDepart : Adresse = Adresse(0,"",0.0,0.0)
                var monAdressedArrive : Adresse = Adresse(0,"",0.0,0.0)
                var monNbPlaceDisponible : Int = resultatTrajet.getJSONObject(i)["nombrePlaceDispo"].toString().toInt()
                var monNbPlaceOccupe : Int = resultatTrajet.getJSONObject(i)["nombrePlaceOccupe"].toString().toInt()
                var typeTrajet : Boolean = resultatTrajet.getJSONObject(i)["nombrePlaceOccupe"].toString().toInt().equals(1)
                var dateTrajet : String = resultatTrajet.getJSONObject(i)["DateDepart"].toString()
                var horaireTrajet : String = resultatTrajet.getJSONObject(i)["horaireDepart"].toString()


                //conducteur,adressedépart,adressearrivé,nbplace
                for(m in 0..listeUtilisateur.size-1){
                    if(resultatTrajet.getJSONObject(i)["idConducteur"].equals(listeUtilisateur.get(m).id)){
                        monConducteur = listeUtilisateur.get(m)
                    }
                }
                for(m in 0..listeAdresse.size-1){
                    if(resultatTrajet.getJSONObject(i)["idAdresseDepart"].toString().toInt().equals(listeAdresse.get(m).id)){
                        monAdressedDepart = listeAdresse.get(m)
                    }
                }
                for(m in 0..listeAdresse.size-1){
                    if(resultatTrajet.getJSONObject(i)["idAdresseArrive"].toString().toInt().equals(listeAdresse.get(m).id)){
                        monAdressedArrive = listeAdresse.get(m)
                    }
                }


                var monTrajet : Trajet = Trajet(monId,monConducteur,monAdressedDepart,monAdressedArrive,monNbPlaceDisponible,monNbPlaceOccupe,typeTrajet,dateTrajet,horaireTrajet)
                listeTrajet.add(monTrajet)

            }

        }

        return listeTrajet
    }
}