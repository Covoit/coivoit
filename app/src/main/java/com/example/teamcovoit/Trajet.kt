package com.example.teamcovoit

import android.os.Build
import androidx.annotation.RequiresApi

class Trajet(val id: Int, val conducteur: Utilisateur, val adrDepart: Adresse, val adrArrive: Adresse, val nbPlacesDisponibles: Int,val nbPlacesOccupes: Int, val typeTrajet: Boolean, val heureDepart: String, val dateDepart : String) {
    var passagers: List<Utilisateur> = emptyList()
    var placesLeft: Int = 0

    init {
        placesLeft = nbPlacesDisponibles
    }

    companion object {
        @RequiresApi(Build.VERSION_CODES.N)
        fun Create(conducteur: Utilisateur, adrDepart: Adresse, adrArrive: Adresse, nbPlacesDisponibles: Int, nbPlacesOccupes: Int, typeTrajet: Boolean, heureDepart: String, dateDepart: String): Trajet {
            var maBDD = GetDonneBDD()
            var monID = maBDD.getMaxTrajets()
            val trajet = Trajet(monID, conducteur, adrDepart, adrArrive, nbPlacesDisponibles,nbPlacesOccupes,typeTrajet,heureDepart, dateDepart)
            return trajet
        }
    }
    @RequiresApi(Build.VERSION_CODES.N)
    fun insert() {

        var testingType: Int = 0
        if (typeTrajet){
            testingType = 1
        }

        var liendeURL: String = "http://student.ddadev.fr/dev_mobile/Projet_Dev/Team_Covoit/API/postTrajet.php?id=${id}&placesDispo=${nbPlacesDisponibles}&placesOccupe=${nbPlacesOccupes}&type=${testingType}&adrDep=${adrDepart.id}&adrArr=${adrArrive.id}&conducteur=${conducteur.id}&date=${dateDepart}&heure=${heureDepart}"
        println("### Lien URL = "+liendeURL)
        var maBDD = GetDonneBDD()
        maBDD.postDonnees(liendeURL)
    }
}