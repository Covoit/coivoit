package com.example.teamcovoit

import android.app.ActionBar
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.PopupWindow
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_accueil.*
import org.jetbrains.anko.*

class AccueilActivity : AppCompatActivity() {

    private var isPopupOpened: Boolean = false

    companion object {
        // Intent à appeler directement depuis une autre classe (companion = attaché à la classe et non à une instance)
        fun getLaunchIntent(from: Context) = Intent(from, AccueilActivity::class.java).apply {
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_accueil)

        val intentPageCovoitMaps =  Intent(this,CovoitMaps::class.java)
        val intentCreationTrajetActivity =  Intent(this,CreationTrajetActivity::class.java)

        // vers la recherche de trajet
        buttonRechercheTrajet.setOnClickListener {
            startActivity(intentPageCovoitMaps)
        }

        // vers la creation de trajet
        buttonProposeTrajet.setOnClickListener {
            startActivity(intentCreationTrajetActivity)
        }


        /*debugInfoUser.setText("Prenom : ${Utilisateur.getUserById(FirebaseAuth.getInstance().currentUser?.uid!!)?.prenom}\n" +
                "Nom : ${Utilisateur.getUserById(FirebaseAuth.getInstance().currentUser?.uid!!)?.nom}\n" +
                "Email : ${Utilisateur.getUserById(FirebaseAuth.getInstance().currentUser?.uid!!)?.email}\n" +
                "Tel : ${Utilisateur.getUserById(FirebaseAuth.getInstance().currentUser?.uid!!)?.tel}")*/

        setupUI()
    }

    // appelé quand l'ui est créée (pour avoir les données chargées)
    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if(hasFocus) {
            // si pas de téléphone renseigné dans Firebase
            if (Utilisateur.getUserById(FirebaseAuth.getInstance().currentUser?.uid!!)?.tel == "tel") {
                showPopup()
            }
        }
    }

    private fun setupUI() {
        sign_out_button.setOnClickListener {
            signOut()
        }
    }

    private fun signOut() {
        startActivity(LoginActivity.getLaunchIntent(this))
        FirebaseAuth.getInstance().signOut()
    }

    // ne fonctionne pas ??
    override fun onBackPressed() {
        if(!isPopupOpened)
        {
            super.onBackPressed()
        }
        else
        {
            return
        }
    }

    // popup quand téléphone non renseigné
    private fun showPopup() {
        isPopupOpened = true
        val popupView : View = layoutInflater.inflate(R.layout.input_phone,null)
        val window = PopupWindow(popupView,ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT)
        window.isFocusable = true

        val btn = popupView.findViewById<Button>(R.id.valider)
        val txt = popupView.findViewById<EditText>(R.id.numeroTel)

        btn.setOnClickListener {
            // si on a bien un format de numéro de téléphone (méthode dans util.kt)
            if(checkPhoneNumber(txt.text.toString()))
            {
                isPopupOpened = false
                // mise à jour dans la base du numéro de téléphone
                Utilisateur.updatePhone(FirebaseAuth.getInstance().currentUser?.uid!!,
                                        Utilisateur.getUserById(FirebaseAuth.getInstance().currentUser?.uid!!)?.prenom!!,
                                        Utilisateur.getUserById(FirebaseAuth.getInstance().currentUser?.uid!!)?.nom!!,
                                        Utilisateur.getUserById(FirebaseAuth.getInstance().currentUser?.uid!!)?.email!!,
                                        txt.text.toString())
                Utilisateur.user.tel = txt.text.toString()
                window.dismiss()
            }
            else
            {
                toast("Invalid phone number")
            }
        }

        //affichage de la fenetre
        window.showAtLocation(this@AccueilActivity.contentView, Gravity.NO_GRAVITY, 0, 0)
    }

}