package com.example.teamcovoit

import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.annotation.RequiresApi
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.auth.OAuthProvider
import kotlinx.android.synthetic.main.activity_login.*
import java.lang.Exception
import java.util.*


class LoginActivity : AppCompatActivity() {
    private val RC_SIGN_IN: Int = 1
    private lateinit var mGoogleSignInClient: GoogleSignInClient
    private lateinit var mGoogleSignInOptions: GoogleSignInOptions
    private lateinit var firebaseAuth: FirebaseAuth
    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val provider = OAuthProvider.newBuilder("microsoft.com")

        configureGoogleSignIn()
        setupUI()

        firebaseAuth = FirebaseAuth.getInstance()

        // click sur bouton connexion microsoft
        exchange.setOnClickListener{
            val pendingResultTask = firebaseAuth.pendingAuthResult
            if (pendingResultTask != null)
            {
                // en cours de connexion -> finalise la connexion
                pendingResultTask
                    .addOnSuccessListener(
                        object:OnSuccessListener<AuthResult> {
                            @RequiresApi(Build.VERSION_CODES.N)
                            override fun onSuccess(authResult:AuthResult) {
                                retrieveUserInfo("exchange")
                                startActivity(AccueilActivity.getLaunchIntent(this@LoginActivity))
                            }
                        })
                    .addOnFailureListener(
                        object:OnFailureListener {
                            override fun onFailure(@NonNull e:Exception) {
                                // Handle failure.
                                Toast.makeText(this@LoginActivity, "Microsoft sign in failed:(", Toast.LENGTH_LONG).show()
                            }
                        })
            }
            else
            {
                // initialize la connexion
                firebaseAuth
                    .startActivityForSignInWithProvider(/* activity= */this, provider.build())
                    .addOnSuccessListener(
                        object:OnSuccessListener<AuthResult> {
                            override fun onSuccess(authResult:AuthResult) {
                                retrieveUserInfo("exchange")
                                startActivity(AccueilActivity.getLaunchIntent(this@LoginActivity))
                            }
                        })
                    .addOnFailureListener(
                        object:OnFailureListener {
                            override fun onFailure(@NonNull e:Exception) {
                                // Handle failure.
                                Toast.makeText(this@LoginActivity, "Microsoft sign in failed:(", Toast.LENGTH_LONG).show()
                            }
                        })
            }
        }

    }

    // intent pour revenir sur cette vue
    companion object {
        fun getLaunchIntent(from: Context) = Intent(from, LoginActivity::class.java).apply {
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        }
    }

    override fun onStart() {
        super.onStart()
        val user = FirebaseAuth.getInstance().currentUser
        // si un utilisateur etait connecté à la derniere utilisation de l'app
        if (user != null) {
            // verif si cet utilisateur est bien enregistré en base
            if(GetDonneBDD().getDonnees("http://student.ddadev.fr/dev_mobile/Projet_Dev/Team_Covoit/API/getUtilisateurById.php?id=${FirebaseAuth.getInstance().currentUser?.uid}").length() != 0)
            {
                // redirection s'il existe pas besoin de se reconnecter
                startActivity(AccueilActivity.getLaunchIntent(this))
                finish()
            }
        }
    }

    private fun configureGoogleSignIn() {
        mGoogleSignInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()
        mGoogleSignInClient = GoogleSignIn.getClient(this, mGoogleSignInOptions)
    }

    private fun setupUI() {
        google_button.setOnClickListener {
            signIn()
        }
    }

    private fun signIn() {
        val signInIntent: Intent = mGoogleSignInClient.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account = task.getResult(ApiException::class.java)
                firebaseAuthWithGoogle(account as GoogleSignInAccount)
            } catch (e: ApiException) {
                Toast.makeText(this, "Google sign in failed:(", Toast.LENGTH_LONG).show()
            }
        }
    }


    @RequiresApi(Build.VERSION_CODES.N)
    private fun firebaseAuthWithGoogle(acct: GoogleSignInAccount) {
        val credential = GoogleAuthProvider.getCredential(acct.idToken, null)
        firebaseAuth.signInWithCredential(credential).addOnCompleteListener {
            if (it.isSuccessful) {
                retrieveUserInfo("google")
                startActivity(AccueilActivity.getLaunchIntent(this))
            } else {
                Toast.makeText(this, "Google sign in failed:(", Toast.LENGTH_LONG).show()
            }
        }
    }
    @RequiresApi(Build.VERSION_CODES.N)
    private fun retrieveUserInfo(orig: String)
    {
        // recuperation des infos de l'utilisateur connecté
        val user = FirebaseAuth.getInstance().currentUser

        if(user != null)
        {
            var uid: String?
            var firstName: String?
            var lastName: String?
            var email: String?
            var tel: String?

            user.let {
                /*
                 *  Google format : Prenom Nom
                 *  Microsoft format : NOM Prenom
                 */
                firstName = when(orig) {
                    "google" -> user.displayName!!.split(' ')[0]
                    "exchange" -> user.displayName!!.split(' ')[1]
                    else -> "first_name"
                }
                lastName = when(orig)
                {
                    "google" -> user.displayName!!.split(' ')[1]
                    "exchange" -> user.displayName!!.split(' ')[0]
                    else -> "last_name"
                }
                email = user.email!!.toLowerCase(Locale.getDefault())
                tel = user.phoneNumber
                uid = user.uid
            }

            if(tel == null){
                // si le téléphone n'est pas renseigné on indique "tel" pour verif apres
                tel = "tel"
            }

            // creation d'un utilisateur (methode dans classe Utilisateur)
            Utilisateur.create(uid!!, firstName!!, lastName!!.upperFirstLetter(), email!!, tel!!)
            //Toast.makeText(this, "New user is connected. First name : $firstName, Last name : $lastName, Email : $email, Phone : $tel", Toast.LENGTH_LONG).show()
        }
        else
        {
            Toast.makeText(this, "No user authenticated", Toast.LENGTH_LONG).show()
        }
    }
}