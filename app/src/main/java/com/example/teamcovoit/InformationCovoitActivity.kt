package com.example.teamcovoit

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.annotation.RequiresApi
import kotlinx.android.synthetic.main.activity_information_covoit.*

class InformationCovoitActivity : AppCompatActivity() {

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_information_covoit)

        this.title = "Information du coivoiturage"

        //récupération du trajet et affichage de celui-ci correctement.
        val nom : String =  this.intent.getStringExtra("nom")
        val prenom : String = this.intent.getStringExtra("prenom")
        val ville : String = this.intent.getStringExtra("ville")
        val placeDispo : Int = this.intent.getIntExtra("placeDispo",0)
        val numTel : String = this.intent.getStringExtra("telephone")

        val nomPrenom : String = nom + "  "+ prenom
        this.textViewNomPrenom.text = nomPrenom
        this.textViewLieuAdresse.text = ville
        this.textViewNombrePlace.text = placeDispo.toString()
        this.textViewNumTel.text =numTel


    }
}
