package com.example.teamcovoit

import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Geocoder
import android.location.Location
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.annotation.RequiresApi

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import org.jetbrains.anko.locationManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.model.BitmapDescriptorFactory


class CovoitMaps : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    var listeTrajet : List<Trajet> = ArrayList()







    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_covoit_maps)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }



    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @SuppressLint("MissingPermission")
    @RequiresApi(Build.VERSION_CODES.N)
    override fun onMapReady(googleMap: GoogleMap) {

        //initialisation
        mMap = googleMap
        var maBDD = GetDonneBDD()

        listeTrajet = maBDD.getTrajets()
        var lm  = locationManager
        var provider : String = "GPS"
        //On demande la permission de se géolocaliser si elle n'est pas déja permise
        val permissions = arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION)
        ActivityCompat.requestPermissions(this, permissions,0)
        val fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        //si on a la permission et qu'on arrive a retrouver la position de l'utilisateur on se positionne dessus et on mets un marker bleue
        if(ContextCompat.checkSelfPermission(this,android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            fusedLocationClient.lastLocation
                .addOnSuccessListener { location: Location? ->
                    if (location != null) {
                        val zoom = 16.0f
                        val positionCourante : LatLng  = LatLng(location.latitude,location.longitude)
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(positionCourante, zoom))
                        mMap.addMarker(MarkerOptions().position(positionCourante).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)))
                    }
                }
        }



        //On mets tous les markers sur la map sauf si le nombre de place est de 0
        if(listeTrajet.isNotEmpty()){
            for(i in 0..listeTrajet.size-1){
                println("place")
                println(listeTrajet.get(i).nbPlacesDisponibles)
                if(listeTrajet.get(i).nbPlacesDisponibles>0){
                    val maPosition : LatLng  = LatLng(listeTrajet.get(i).adrDepart.lat,listeTrajet.get(i).adrDepart.long)
                    mMap.addMarker(MarkerOptions().position(maPosition).title(listeTrajet.get(i).adrDepart.ville))
                }
            }
        }



        //Quand on click sur un marker on zoom a une distance suffisante pour se situer
        mMap!!.setOnMarkerClickListener(object : GoogleMap.OnMarkerClickListener {
            override fun onMarkerClick(marker: Marker): Boolean {
                //Zoom quand on clique sur un marker
                val zoom = 16.0f
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(marker.position, zoom))
                return false
            }
        })

        //On récupère les données du marker cliqué puis on les envoie a la page information
        mMap.setOnInfoWindowClickListener(GoogleMap.OnInfoWindowClickListener {
            val intentPageInfoTrajet : Intent =  Intent(this,InformationCovoitActivity::class.java)
            //Se rend sur la fiche du trajet quand on clique sur l'info du marker
            if(listeTrajet.size>=it.id.get(1).toString().toInt()){

                val idTrajet = it.id.get(1).toString().toInt()
                val nom :String = listeTrajet.get(idTrajet).conducteur.nom
                val prenom :String = listeTrajet.get(idTrajet).conducteur.prenom
                val ville :String = listeTrajet.get(idTrajet).adrDepart.ville
                val placeDispo : Int = listeTrajet.get(idTrajet).nbPlacesDisponibles
                val telephone : String = listeTrajet.get(idTrajet).conducteur.tel

                intentPageInfoTrajet.putExtra("nom",nom)
                intentPageInfoTrajet.putExtra("prenom",prenom)
                intentPageInfoTrajet.putExtra("ville",ville)
                intentPageInfoTrajet.putExtra("placeDispo",placeDispo)
                intentPageInfoTrajet.putExtra("telephone",telephone)
                startActivity(intentPageInfoTrajet)

            }



        })



    }

















    }
