package com.example.teamcovoit

import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.annotation.RequiresApi
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_creation_trajet.*
import kotlinx.android.synthetic.main.activity_recherche_trajet.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.okButton
import org.json.JSONArray
import java.sql.Date
import java.sql.Time

class CreationTrajetActivity : AppCompatActivity() {

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_creation_trajet)
        this.title = "Creation d'un Trajet"


        // Action lors du clique sur le bouton Creation Trajet
        buttonCreationTrajet.setOnClickListener{

            val intentPageAccueil = Intent(this, AccueilActivity::class.java)

            // récupération des données saisis pas l'utilisateur
            var villeDepart: String = editTextClasseVille.text.toString()
            var horaireDepart: String = editTextHoraire.text.toString()
            var dateDepart: String = editTextDateDepart.text.toString()
            var nombrePlaces: Int = 0
            if(editTextNbPlaces.text.toString().toIntOrNull() != null){
                nombrePlaces = editTextNbPlaces.text.toString().toIntOrNull()!!
            }

            var maBDD = GetDonneBDD()
            println("###"+villeDepart)
            val geoCoder = Geocoder(this)
            // récupération des données GPS en fonction de l'adresse renseignée
            val Destination = geoCoder.getFromLocationName(villeDepart,1)

            if(!Destination.isEmpty()){
                // récupération de la latitude et longitude pour y afficher sur la map google.
                val latitudeAdresse: Double = Destination.get(0).latitude
                val longitudeAdresse: Double =Destination.get(0).longitude
                var monAdresse: Adresse = Adresse.Create(villeDepart,latitudeAdresse,longitudeAdresse)
                // insertion de l'adresse dans la base de données
                monAdresse.insert()
                val maximumID: Int = maBDD.getMaxAdresses()
                println("### Insertion adresse reussi")
                //création d'un Utilisateur
                val idConducteur: Utilisateur = Utilisateur.getUserById(FirebaseAuth.getInstance().currentUser?.uid!!)!!

                var monTrajet: Trajet = Trajet.Create(idConducteur, monAdresse, monAdresse, nombrePlaces, 0,true, horaireDepart, dateDepart)
                // insertion de l'adresse dans la base de données
                monTrajet.insert()

                // retour sur la page d'accueil après création du Trajet
                startActivity(intentPageAccueil)
            }
            else
            {
                // message d'alerte avant création du trajet si celui-ci possède une adresse non trouvée
                alert(message = "Adresse non trouvée", title = "Erreur") {
                    okButton {  }
                }.show()
            }


        }

    }
}
