package com.example.teamcovoit

import android.os.Build
import androidx.annotation.RequiresApi

class Adresse(val id: Int, val ville: String, val lat: Double, val long: Double) {
    init {
        println("New address created : $ville, Coordinates : $lat,$long")
    }

    companion object {
        @RequiresApi(Build.VERSION_CODES.N)
        fun Create(ville: String, lat: Double, long: Double): Adresse {
            var maBDD = GetDonneBDD()
            var monID = maBDD.getMaxAdresses()
            val adresse = Adresse(monID, ville, lat, long)
            return adresse
        }
    }
        @RequiresApi(Build.VERSION_CODES.N)
        fun insert() {
            var liendeURL: String = "http://student.ddadev.fr/dev_mobile/Projet_Dev/Team_Covoit/API/setNewAdresse.php?id_adresse=${id}&positionLat=${lat}&positionLong=${long}&ville=${ville}"
            var maBDD = GetDonneBDD()
            maBDD.postDonnees(liendeURL)
        }

}