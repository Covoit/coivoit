package com.example.teamcovoit

// methode pour mettre en maj la premiere lettre d'un string (similaire a capitalize() qui ne fonctionne pas)
fun String.upperFirstLetter(): String
{
    // array tampon pour sortie nouveau string
    val strArray = CharArray(this.length)

    // conversion du string en tableau de char
    val x = this.toCharArray()

    // on parcours chaque caractere du string
    x.forEachIndexed{index,element ->
        println("char $element -- index $index")
        // premiere lettre en majuscule
        if(index == 0){
            strArray[index]=element.toUpperCase()
        // les autres en minuscule
        }else{
            strArray[index]=element.toLowerCase()
        }

    }

    // on recréé le string a partir du tableau de char
    return strArray.joinToString("")
}

// verification numero de telephone
fun checkPhoneNumber(num: String): Boolean
{
    /*
     * Taille = 10
     * Composé uniquement de chiffres
     * Commence par 06 ou 07 uniquement (pas de telephone fixe)
     */
    return num.length == 10 && num.toIntOrNull() != null && (num.substring(0,2) == "06" || num.substring(0,2) == "07")
}